package com.sama.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.sama.myapplication.R;
import com.sama.myapplication.model.UserModel;
import com.sama.myapplication.util.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CandidateDetailsActivity extends BaseActivity {

    UserModel userModel = new UserModel();
    @BindView(R.id.tvFullName)
    TextView tvFullName;
    @BindView(R.id.tvEmailAddress)
    TextView tvEmailAddress;
    @BindView(R.id.tvPhoneNumber)
    TextView tvPhoneNumber;
    @BindView(R.id.tvCity)
    TextView tvCity;
    @BindView(R.id.tvLanguage)
    TextView tvLanguage;
    @BindView(R.id.tvHobbies)
    TextView tvHobbies;
    @BindView(R.id.btnUpdate)
    Button btnUpdate;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvDob)
    TextView tvDob;
    @BindView(R.id.tvDobTime)
    TextView tvDobTime;
    @BindView(R.id.tvAge)
    TextView tvAge;

    private float x1, x2;
    static final int MIN_DISTANCE = 150;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_details);
        ButterKnife.bind(this);
        setUpActionBar("Details", false);
        position = getIntent().getIntExtra("POSITION", -1);
        bindValues(position);
    }

    void bindValues(int position) {

        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            ;
            tvFullName.setText(userModel.getUserName() + " " + userModel.getUserFatherName() + " " + userModel.getUserSurname());
            tvEmailAddress.setText(userModel.getUserEmailAddress());
            tvPhoneNumber.setText(userModel.getPhoneNumber());
            tvCity.setText(userModel.getCity());
            tvDob.setText(userModel.getUserDob());
            tvDobTime.setText(userModel.getUserDobTime());
            tvAge.setText(userModel.getAge());
            tvLanguage.setText(userModel.getLanguage());
            tvHobbies.setText(userModel.getUserHobbies());

            btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CandidateDetailsActivity.this, AddCandidateActivity.class);
                    intent.putExtra(Constant.USER_OBJECT, userModel);
                    startActivity(intent);
                }
            });
        }
    }

    /*@Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                float deltaX = x2 - x1;
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    // Left to Right swipe action
                    if (x2 > x1) {
                        if (position > 0) {
                            bindValues(--position);
                        } else {
                            Toast.makeText(this, "Sorry No Candidate Found", Toast.LENGTH_SHORT).show();
                        }

                    }

                    // Right to left swipe action
                    else {
                        if (position < userList.size() - 1) {
                            bindValues(++position);
                        } else {
                            Toast.makeText(this, "Sorry No Candidate Found", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    // consider as something else - a screen tap for example
                }
                break;
        }
        return super.onTouchEvent(event);
    }*/
}