package com.sama.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sama.myapplication.R;
import com.sama.myapplication.model.UserCityModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CityAdapter extends BaseAdapter {

    Context context;
    ArrayList<UserCityModel> userCityList;

    @Override
    public int getCount() {
        return userCityList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = view;
        ViewHolder viewHolder;
        if (v==null){
            v = LayoutInflater.from(context).inflate(R.layout.view_row_text, null);
            viewHolder = new ViewHolder(v);
            v.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) v.getTag();
        }

        viewHolder.tvName.setText(userCityList.get(i).getName());

        return v;
    }

    static
    class ViewHolder {
        @BindView(R.id.tvName)
        TextView tvName;
        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
