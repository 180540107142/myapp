package com.sama.myapplication.database.tables;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sama.myapplication.database.MyDatabase;
import com.sama.myapplication.model.UserLanguageModel;

import java.util.ArrayList;

public class UserLanguageTbl extends MyDatabase {

    public static final String TABLE_NAME = "UserLanguageTbl";
    public static final String USER_LANGUAGE_ID = "UserLanguageId";
    public static final String NAME = "Name";

    public UserLanguageTbl(Context context) {
        super(context);
    }

    public ArrayList<UserLanguageModel> getLanguageList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserLanguageModel> languageList = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        for (int i = 0; i < cursor.getCount(); i++) {
            UserLanguageModel userLanguageList = new UserLanguageModel();

            userLanguageList.setUserLanguageId(cursor.getInt(cursor.getColumnIndex(USER_LANGUAGE_ID)));
            userLanguageList.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            languageList.add(userLanguageList);
            cursor.moveToNext();
        }

        cursor.close();
        db.close();

        return languageList;
    }
}
