package com.sama.myapplication.model;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserCityModel implements Serializable {

    int UserCityId;
    String Name;


    public int getUserCityId() {
        return UserCityId;
    }

    public void setUserCityId(int userCityId) {
        UserCityId = userCityId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "UserCityModel{" +
                "UserCityId=" + UserCityId +
                ", Name='" + Name + '\'' +
                '}';
    }
}
