package com.sama.myapplication.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserModel implements Serializable {

    int UserId;
    String UserName;
    String UserFatherName;
    String UserSurname;
    int UserGender;
    String UserHobbies;
    String UserDob;
    String UserEmailAddress;
    String PhoneNumber;
    int UserLanguageId;
    int UserCityId;
    String Language;
    String City;
    int IsFavorite;
    String Age;
    String UserDobTime;

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserFatherName() {
        return UserFatherName;
    }

    public void setUserFatherName(String userFatherName) {
        UserFatherName = userFatherName;
    }

    public String getUserSurname() {
        return UserSurname;
    }

    public void setUserSurname(String userSurname) {
        UserSurname = userSurname;
    }

    public int getUserGender() {
        return UserGender;
    }

    public void setUserGender(int userGender) {
        UserGender = userGender;
    }

    public String getUserHobbies() {
        return UserHobbies;
    }

    public void setUserHobbies(String userHobbies) {
        UserHobbies = userHobbies;
    }

    public String getUserDob() {
        return UserDob;
    }

    public void setUserDob(String userDob) {
        UserDob = userDob;
    }

    public String getUserEmailAddress() {
        return UserEmailAddress;
    }

    public void setUserEmailAddress(String userEmailAddress) {
        UserEmailAddress = userEmailAddress;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public int getUserLanguageId() {
        return UserLanguageId;
    }

    public void setUserLanguageId(int userLanguageId) {
        UserLanguageId = userLanguageId;
    }

    public int getUserCityId() {
        return UserCityId;
    }

    public void setUserCityId(int userCityId) {
        UserCityId = userCityId;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public int getIsFavorite() {
        return IsFavorite;
    }

    public void setIsFavorite(int isFavorite) {
        IsFavorite = isFavorite;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getUserDobTime() {
        return UserDobTime;
    }

    public void setUserDobTime(String userDobTime) {
        UserDobTime = userDobTime;
    }


    @Override
    public String toString() {
        return "UserModel{" +
                "UserId=" + UserId +
                ", UserName='" + UserName + '\'' +
                ", UserFatherName='" + UserFatherName + '\'' +
                ", UserSurname='" + UserSurname + '\'' +
                ", UserGender=" + UserGender +
                ", UserHobbies='" + UserHobbies + '\'' +
                ", UserDob='" + UserDob + '\'' +
                ", UserEmailAddress='" + UserEmailAddress + '\'' +
                ", PhoneNumber='" + PhoneNumber + '\'' +
                ", UserLanguageId=" + UserLanguageId +
                ", UserCityId=" + UserCityId +
                ", Language='" + Language + '\'' +
                ", City='" + City + '\'' +
                ", IsFavorite=" + IsFavorite +
                ", Age='" + Age + '\'' +
                ", UserDobTime='" + UserDobTime + '\'' +
                '}';
    }
}
